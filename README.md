# SLAM Analysis

## Setting things up
1. Create a `fiducial_ws` which contains the [fiducial_slam](https://wiki.ros.org/fiducial_slam#fiducial_slam-1) and [vision_msgs](https://github.com/ros-perception/vision_msgs) packages in the `src` folder. 
2. Move `fiducial_slam_trisect.launch` into the launch folder of fiducial_slam and `aruco_detect_trisect.launch` into the launch folder of aruco_detect.
3. Download and unzip [cropped.zip](https://drive.google.com/file/d/17LmwHYTL9qGDQfluPJBZntUvTHGnEQLK/view?usp=drive_link)


## Collecting fiducialSLAM data
Here are the terminal tabs I usually have open:
1. `roscore`
2. `rosrun tf static_transform_publisher 0 0 0 0 0 0 map left 50` \
    1. `rosparam set /use_sim_time true`
3. `roslaunch fiducial_rviz.launch` 
4. `roslaunch fiducial_slam_trisect.launch` (alternating with `rm /home/{your username}/.ros/slam/map.txt`)
5. `roslaunch aruco_detect_trisect.launch`
6. `rosbag play --clock cropped.bag` 
